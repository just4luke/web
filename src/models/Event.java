package models;

/**
 * Created by Luke on 21/03/2016.
 */

public class Event {
    private String eventID;
    private String eventDate;
    private String startTime;
    private String endTime;
    private String standingTickets;
    private String firstTierTickets;
    private String secondTierTickets;
    private String venueID;
    private String tourID;

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStandingTickets() {
        return standingTickets;
    }

    public void setStandingTickets(String standingTickets) {
        this.standingTickets = standingTickets;
    }

    public String getFirstTierTickets() {
        return firstTierTickets;
    }

    public void setFirstTierTickets(String firstTierTickets) {
        this.firstTierTickets = firstTierTickets;
    }

    public String getSecondTierTickets() {
        return secondTierTickets;
    }

    public void setSecondTierTickets(String secondTierTickets) {
        this.secondTierTickets = secondTierTickets;
    }

    public String getVenueID() {
        return venueID;
    }

    public void setVenueID(String venueID) {
        this.venueID = venueID;
    }

    public String getTourID() {
        return tourID;
    }

    public void setTourID(String tourID) {
        this.tourID = tourID;
    }
}
