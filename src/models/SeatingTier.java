package models;

public class SeatingTier {
    private String tierID;
    private String ticketPrice;
    private String tierName;

    public String getTierID() {
        return tierID;
    }

    public void setTierID(String tierID) {
        this.tierID = tierID;
    }

    public String getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(String ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getTierName() {
        return tierName;
    }

    public void setTierName(String tierName) {
        this.tierName = tierName;
    }
}
