package models;

public class Ticket {
    private String ticketID;
    private String tierID;
    private String bookingID;
    private String eventID;

    public String getTicketID() {
        return ticketID;
    }

    public void setTicketID(String ticketID) {
        this.ticketID = ticketID;
    }

    public String getTierID() {
        return tierID;
    }

    public void setTierID(String tierID) {
        this.tierID = tierID;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }
}
