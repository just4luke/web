package models;

/**
 * Created by Luke on 21/03/2016.
 */
public class Venue {
    private String venueID;
    private String venueName;
    private String venueDescription;
    private String standingCapacity;
    private String firstTierCapacity;
    private String secondTierCapacity;
    private String countyID;
    private String city;
    private String addressLine1;
    private String addressLine2;
    private String postcode;
    private String telephoneNumber;

    public String getVenueID() {
        return venueID;
    }

    public void setVenueID(String venueID) {
        this.venueID = venueID;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueDescription() {
        return venueDescription;
    }

    public void setVenueDescription(String venueDescription) {
        this.venueDescription = venueDescription;
    }

    public String getStandingCapacity() {
        return standingCapacity;
    }

    public void setStandingCapacity(String standingCapacity) {
        this.standingCapacity = standingCapacity;
    }

    public String getCountyID() {
        return countyID;
    }

    public void setCountyID(String countyID) {
        this.countyID = countyID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getFirstTierCapacity() {
        return firstTierCapacity;
    }

    public void setFirstTierCapacity(String firstTierCapacity) {
        this.firstTierCapacity = firstTierCapacity;
    }

    public String getSecondTierCapacity() {
        return secondTierCapacity;
    }

    public void setSecondTierCapacity(String secondTierCapacity) {
        this.secondTierCapacity = secondTierCapacity;
    }
}
