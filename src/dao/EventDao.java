package dao;

import models.Event;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by Luke on 21/03/2016.
 */
public class EventDao {
    public static Event getEvent(String tourID) {
        Event event = new Event();

        try {
            Connection connection = ConnectionProvider.getConnection();

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM EVENTS WHERE TOURID=?");

            stmt.setString(1, tourID);

            ResultSet rs = stmt.executeQuery();
            rs.next();
            event.setTourID(tourID);
            event.setEventID(rs.getString("EVENTID"));

            System.out.println("test");
            event.setEndTime(rs.getString("ENDTIME"));
            event.setEventDate(rs.getString("EVENTDATE"));
            event.setFirstTierTickets(rs.getString("FIRSTTIERTICKETS"));
            event.setSecondTierTickets(rs.getString("SECONDTIERTICKETS"));
            event.setStandingTickets(rs.getString("STANDINGTICKETS"));
            event.setStartTime(rs.getString("STARTTIME"));
            event.setVenueID(rs.getString("VENUEID"));
        } catch (Exception e) {}
        return event;
    }

    public static Event getEventByID(String eventID){
        Event event = new Event();

        try {
            Connection connection = ConnectionProvider.getConnection();

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM EVENTS WHERE EVENTID=?");

            stmt.setString(1, eventID);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            event.setEventID(rs.getString("EVENTID"));
            event.setEventDate(rs.getString("EVENTDATE"));
            event.setStartTime(rs.getString("STARTTIME"));
            event.setEndTime(rs.getString("ENDTIME"));
            event.setStandingTickets(rs.getString("STANDINGTICKETS"));
            event.setFirstTierTickets(rs.getString("FIRSTTIERTICKETS"));
            event.setSecondTierTickets(rs.getString("SECONDTIERTICKETS"));
            event.setVenueID(rs.getString("VENUEID"));
            event.setTourID(rs.getString("TOURID"));

        } catch (Exception e) {}
        return event;
    }
}
