package dao;

import models.SeatingTier;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SeatingTiersDao {

    public static SeatingTier getSeatingTierByID(String seatingTierID){
        SeatingTier seatingTier = new SeatingTier();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM SEATINGTIERS WHERE TIERID=?");

            stmt.setString(1, seatingTierID);

            ResultSet rs = stmt.executeQuery();
            rs.next();
            seatingTier.setTierID(rs.getString("TIERID"));
            seatingTier.setTicketPrice(rs.getString("TICKETPRICE"));
            seatingTier.setTierName(rs.getString("TIERNAME"));

        } catch (Exception e) {}
        return seatingTier;
    }

}
