package dao;

import models.Booking;
import models.History;
import models.Ticket;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class BookingsDao {

    public static ArrayList<Booking> getBookingsByCustomerID(String customerID){
        ArrayList<Booking> bookings = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM BOOKINGS WHERE CUSTOMERID=?");
            stmt.setString(1, customerID);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Booking booking = new Booking();
                booking.setCustomerID(rs.getString("CUSTOMERID"));
                booking.setBookingID(rs.getString("BOOKINGID"));
                booking.setDatePurchased(rs.getString("DATEPURCHASED"));
                booking.setTotalCost(rs.getString("TOTALCOST"));
                bookings.add(booking);
            }
            rs.close();
        }catch (Exception e) {}
        return bookings;
    }

    public static ArrayList<Ticket> getTicketsByBookingID(String bookingID){
        ArrayList<Ticket> tickets = new ArrayList<>();
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM TICKETS WHERE BOOKINGID=?");
            stmt.setString(1, bookingID);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Ticket ticket = new Ticket();
                ticket.setBookingID(rs.getString("BOOKINGID"));
                ticket.setEventID(rs.getString("EVENTID"));
                ticket.setTicketID(rs.getString("TICKETID"));
                ticket.setTierID(rs.getString("TIERID"));
                tickets.add(ticket);
            }
            rs.close();
        }catch (Exception e) {}
        return tickets;
    }

    public static ArrayList<History> getHistoriesByCustomerID(String customerID){
        ArrayList<History> histories = new ArrayList<>();

        ArrayList<Booking> bookings = getBookingsByCustomerID(customerID);
        for(Booking booking : bookings){
            History history = new History();
            history.setBooking(booking);
            history.setTickets(getTicketsByBookingID(booking.getBookingID()));
            history.setTourName(TourDao.getTourByID(EventDao.getEventByID(history.getTickets().get(0).getEventID()).getTourID()).getTourName());
            histories.add(history);
        }

        return histories;
    }
}
