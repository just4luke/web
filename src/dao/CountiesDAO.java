package dao;

import models.County;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

public class CountiesDao {

    public static ArrayList<County> getCounties(){
        ArrayList<County> counties = new ArrayList<>();

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM COUNTIES");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                County county = new County();
                county.setCountyID(rs.getString("COUNTYID"));
                county.setCountyName(rs.getString("COUNTYNAME"));
                counties.add(county);
            }
            rs.close();

        } catch (Exception e) {}
        return counties;
    }
}
