package dao;

import models.Tour;
import providers.ConnectionProvider;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 * Created by Luke on 21/03/2016.
 */
public class TourDao {
    public static Tour getTour(String artistID) {
        Tour tour = new Tour();
        try {
            Connection connection = ConnectionProvider.getConnection();
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM TOURS WHERE ARTISTID=?");

        stmt.setString(1, artistID);

        ResultSet rs = stmt.executeQuery();
            rs.next();


                tour.setArtistID(artistID);
                tour.setTourID(rs.getString("TOURID"));
                tour.setStartDate(rs.getString("STARTDATE"));
                tour.setEndDate(rs.getString("ENDDATE"));
                tour.setTourDescription(rs.getString("TOURDESCRIPTION"));
                tour.setTourName(rs.getString("TOURNAME"));

    } catch (Exception e) {}
    return tour;
}


    public static Tour getTourByID(String tourID){

        Tour tour = new Tour();

        try {
            Connection connection = ConnectionProvider.getConnection();

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM TOURS WHERE TOURID=?");

            stmt.setString(1, tourID);

            ResultSet rs = stmt.executeQuery();
            rs.next();
            tour.setTourID(rs.getString("TOURID"));
            tour.setArtistID(rs.getString("ARTISTID"));
            tour.setTourName(rs.getString("TOURNAME"));
            tour.setStartDate(rs.getString("STARTDATE"));
            tour.setEndDate(rs.getString("ENDDATE"));
            tour.setTourDescription("TOURDESCRIPTION");

        } catch (Exception e) {}
        return tour;
    }
}
