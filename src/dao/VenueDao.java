package dao;

import models.Venue;
import providers.ConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by Luke on 21/03/2016.
 */
public class VenueDao {
    public static Venue getVenue(String venueID) {
        Venue venue = new Venue();

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM VENUES WHERE VENUEID=?");

            stmt.setString(1, venueID);

            ResultSet rs = stmt.executeQuery();
            rs.next();
            venue.setVenueID(venueID);

            System.out.println("test");
            venue.setAddressLine1(rs.getString("ADDRESSLINE1"));
            venue.setAddressLine2(rs.getString("ADDRESSLINE2"));
            venue.setCity(rs.getString("CITY"));
            venue.setCountyID(rs.getString("COUNTYID"));
            venue.setPostcode(rs.getString("POSTCODE"));
            venue.setStandingCapacity(rs.getString("STANDINGCAPACITY"));
            venue.setFirstTierCapacity(rs.getString("FIRSTTIERCAPACITY"));
            venue.setSecondTierCapacity(rs.getString("SECONDTIERCAPACITY"));
            venue.setVenueDescription(rs.getString("VENUEDESCRIPTION"));
            venue.setVenueName(rs.getString("VENUENAME"));
            venue.setTelephoneNumber(rs.getString("TELEPHONENUMBER"));

        } catch (Exception e) {}
        return venue;
    }
}
