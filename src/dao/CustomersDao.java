package dao;
import models.Customer;
import providers.ConnectionProvider;
import login.LoginDetails;
import models.User;

import java.sql.*;
import java.util.logging.Logger;

public class CustomersDao {

    public static int getNextUserID(){
        int count = -1;
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT MAX(USERID) AS maxid FROM USERS");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt("maxid") + 1;
            rs.close();

        } catch (Exception e) {}
        return count;
    }

    public static boolean register(User user, Customer customer){
        boolean userStatus = false;
        boolean customerStatus = false;

        try {
            Connection connection = ConnectionProvider.getConnection();

            PreparedStatement stmtCustomer = connection.prepareStatement("INSERT INTO CUSTOMERS VALUES(?,?,?,?,?,?,?,?,?) ");
            stmtCustomer.setString(1, customer.getCustomerID());
            stmtCustomer.setString(2, customer.getCustomerFirstName());
            stmtCustomer.setString(3, customer.getCustomerLastName());
            stmtCustomer.setString(4, customer.getCustomerEmail());
            stmtCustomer.setString(5, customer.getCountyID());
            stmtCustomer.setString(6, customer.getCity());
            stmtCustomer.setString(7, customer.getAddressLine1());
            stmtCustomer.setString(8, customer.getAddressLine2());
            stmtCustomer.setString(9, customer.getPostcode());
            ResultSet rsCustomer = stmtCustomer.executeQuery();
            customerStatus = rsCustomer.next();
            rsCustomer.close();

            PreparedStatement stmtUser = connection.prepareStatement("INSERT INTO USERS VALUES(?,?,?,?,?,?) ");
            stmtUser.setString(1, user.getUserID());
            stmtUser.setString(2, user.getUsername());
            stmtUser.setString(3, user.getPassword());
            stmtUser.setString(4, user.getCustomerID());
            stmtUser.setString(5, user.getRoleID());
            stmtUser.setString(6, user.getEmail());
            ResultSet rsUser = stmtUser.executeQuery();
            userStatus = rsUser.next();
            rsUser.close();

        } catch (Exception e) {}
        if(customerStatus && userStatus)return true;
        else return false;
    }


    public static boolean validate(LoginDetails details) {
        boolean status = false;

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Users WHERE USERNAME=? AND PASSWORD=?");

            stmt.setString(1, details.getUsername());
            stmt.setString(2, details.getPassword());

            ResultSet rs = stmt.executeQuery();
            status = rs.next();
            rs.close();
        } catch (Exception e) {}

        return status;
    }

    public static int getCustomerIDByUserName(String username){
        int customerID = -1;
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT CUSTOMERID FROM USERS WHERE USERNAME = ?");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            customerID = rs.getInt("CUSTOMERID");
            rs.close();

        } catch (Exception e) {}
        return customerID;
    }

    public static int getNextCustomerID() {
        int count = -1;
        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT MAX(CUSTOMERID) AS rowcount FROM CUSTOMERS");
            ResultSet rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt("rowcount") + 1;
            rs.close();

        } catch (Exception e) {}
        return count;
    }


}
