package dao;
import providers.ConnectionProvider;
import models.Artist;

import java.sql.*;

public class ArtistDao {

    public static Artist getArtist(String artistId) {
        Artist artist = new Artist();

        try {
            Connection connection = ConnectionProvider.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ARTISTS WHERE ARTISTID=?");

            stmt.setString(1, artistId);

            ResultSet rs = stmt.executeQuery();
            rs.next();
            artist.setArtistId(artistId);
            artist.setArtistName(rs.getString("ARTISTNAME"));
            System.out.println("test");
            artist.setArtistDescription(rs.getString("ARTISTDESCRIPTION"));
            artist.setGenreId(rs.getString("GENREID"));

        } catch (Exception e) {}
            return artist;
    }
}
