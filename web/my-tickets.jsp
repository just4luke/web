<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="dao.BookingsDao" %>
<%@ page import="models.History" %>
<%@ page import="dao.CustomersDao" %>
<%@ page import="models.SeatingTier" %>
<%@ page import="dao.SeatingTiersDao" %>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/site.css">
  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.css">
  <script src="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.min.js"></script>
  <script type="text/javascript" src="js/site.js"></script>

</head>
<body onload="loadAuto()">
<%
  if(session.getAttribute("userName") == null){
%>
<meta http-equiv="Refresh" content="0;url=login.jsp">
<%
  }else{
%>
<%@ include file="nav-bar.jsp" %>
<h3 style="text-align:center">My Tickets: Order History</h3>
<div class="order-history-wrapper">
  <div class="order-history-header">
    <div class="order-history-div0">
      <a>Order Number:</a>
    </div>
    <div class="order-history-div1">
      <a>Order Date:</a>
    </div>
    <div class="order-history-div2">
      <a>Tour:</a>
    </div>
    <div class="order-history-div3">
      <a>Seat Type:</a>
    </div>
    <div class="order-history-div4">
      <a>Order Cost:</a>
    </div>
  </div>
  <ul class="order-history-ul">
    <%
      for(History history : BookingsDao.getHistoriesByCustomerID(Integer.toString(CustomersDao.getCustomerIDByUserName(session.getAttribute("userName").toString())))){
    %>
      <li class="order-li">
        <div class="order-history-div0">
          <a><%=history.getBooking().getBookingID()%></a>
        </div>
        <div class="order-history-div1">
          <a><%=history.getBooking().getDatePurchased()%></a>
        </div>
        <div class="order-history-div2">
          <a><%=history.getTourName()%></a>
        </div>
        <div class="order-history-div3">
          <a><%=SeatingTiersDao.getSeatingTierByID(history.getTickets().get(0).getTierID()).getTierName()%></a>
        </div>
        <div class="order-history-div4">
          <a>£<%=history.getBooking().getTotalCost()%>0</a>
        </div>
      </li>
    <%
      }
    %>
  </ul>
</div>
<%
  }
%>
</body>
<html>
