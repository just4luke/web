function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace(
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;
	}
	return vars;
}

function search(){
	if(document.getElementById("autocomplete").value.trim().length > 0)
	open('search-results.html?q=' + document.getElementById("autocomplete").value.trim() , "_self");
}

function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}

function loadAuto(){
$('#autocomplete').autocomplete({
    lookup: function (query, done) {
        // Do ajax call or lookup locally, when done,
        // call the callback and pass your results:
        var result = {
            suggestions: [
				
				{ "value": "Plymouth", "data": {catergory: 'City'} },
				{ "value": "Peter Andre", "data": {catergory: 'Artist'} },
            ]
        };

        done(result);
    },
    onSelect: function (suggestion) {
        open('search-results.html?q=' + suggestion.value, "_self");
    },
	groupBy: 'catergory',
	showNoSuggestionNotice: true,
	forceFixPosition: true,
	maxHeight : 200
	
});

if(localStorage.getItem("user") === null){
	$( "#log-out" ).css('display', 'none');
}
	getUser();
}

$(document).ready(function() {
	$('.slider').jcider({
	visibleSlides: 3,
	autoplay: true,
	looping: true
	});
});

function getUser() {
    if(typeof(Storage) !== "undefined") {
	if(localStorage.getItem("user") === null){
	    document.getElementById("user").innerHTML = "<a href='log-in.html'>Log In</a>";
	}else{
        document.getElementById("user").innerHTML = localStorage.getItem('user') + '  <a href="" style="padding-left:5px;" onclick="logOut()">Log Out</a>';
   }
   } else {
        document.getElementById("user").innerHTML = "Sorry, your browser does not support web storage...";
    }
}

function logIn(){
	if(document.getElementById("username").value === "PRCS251A"){
		if(document.getElementById("password").value === "PRCS251A"){
			localStorage.setItem('user', document.getElementById("username").value);
			getUser();
			open('index.html', "_self");
		}
	}
}

	function checkLogIn(){
		if(localStorage.getItem("user") === null){
			open('log-in.html', '_self');
		}
	}

function logOut(){
	localStorage.clear();
	localStorage.removeItem("user");
}