<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/site.css">
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.css">
    <script src="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.min.js"></script>
    <script type="text/javascript" src="js/site.js"></script>
    <title>Login</title>
</head>
<body>
<%@ include file="nav-bar.jsp" %>
<br/>

<div class="main-content" id="login-register-content">
    <div class="login-register-tab-container">

    </div>
    <h3>Login</h3>
    <form action="loginprocess.jsp" method="post">
        Username: <input type="text" name="username"><br/><br/>
        Password: <input type="password" name="password"><br/><br/>
        <input type="submit" value="Login">
        <input type="button" value="Register" onclick="location.href='register.jsp'">
    </form>
</div>

</body>
</html>
