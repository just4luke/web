<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="dao.ArtistDao" %>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/site.css">
  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.css">
  <script src="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.min.js"></script>
  <script type="text/javascript" src="js/site.js"></script>

</head>
<body onload="loadAuto()">
<%@ include file="nav-bar.jsp" %>
<%@ include file="slider.html" %>
</body>
</html>
