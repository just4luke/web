<%@ page import="dao.ArtistDao" %>
<%@ page import="dao.TourDao" %>
<%@ page import="dao.EventDao" %>
<%@ page import="dao.VenueDao" %>
<%@ page import="models.Tour" %>
<jsp:useBean id="artist" class="models.Artist" scope="application"/>
<jsp:useBean id="tour" class="models.Tour" scope="application"/>
<jsp:useBean id="event" class="models.Event" scope="application"/>
<jsp:useBean id="venue" class="models.Venue" scope="application"/>

<%
    artist = ArtistDao.getArtist(request.getParameter("id"));
    tour = TourDao.getTour(request.getParameter("id"));
    event = EventDao.getEvent(tour.getTourID());
    venue = VenueDao.getVenue(event.getVenueID());

%>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/site.css">
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.css">
    <script src="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.min.js"></script>
    <script type="text/javascript" src="js/site.js"></script>
    <title>Artist - <%=artist.getArtistName()%></title>
</head>
<body>
<%@ include file="nav-bar.jsp" %>
<div class="tour-content">
    <h3><%=artist.getArtistName()%></h3>
    <h4></h4>
        <div class="tour-banner">
            <div class="tour-image-area">
                <img class="tour-image" src="images/PA_460x260_Newcastle1.jpg">
            </div>
            <div class="tour-info">
                <p>
                    <%=artist.getArtistDescription()%>
                </p>
            </div>
        </div>


        <table class="artisttable">
            <tr>
                <th/>
                <th>Event</th>
                <th>Location</th>
                <th>Date</th>
                <th/>
            </tr>

            //iterate all events with specific tour id for this artist
            <%
                //for(){
            %>
            <tr>
                <%--<td><img class="event-image-thumb" src="images/Peter-Andre-Thumbnail.jpg"></td>--%>
                <td>placeholder image</td>
                <td><%=tour.getTourName()%></td>
                <td><%=venue.getAddressLine1()%> <%if(venue.getAddressLine2() != null){venue.getAddressLine2();}%><br/><%=venue.getCity()%></td>
                <td>date:<%=event.getEventDate()%><br/>start time:<%=event.getStartTime()%></td>
                <td><input class = "nav-search-button" type="button" name = "view-tickets" value="View Tickets" onclick="clickCounter()" style="float:right; margin-top:70px"></td>
            </tr>
            <%
                }
            %>

        </table>
    <br/>
</div>

</body>
</html>
