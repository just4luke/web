<%@ page import="dao.CustomersDao" %>
<%@ page import="models.User" %>
<%@ page import="models.Customer" %>
<%@ page import="java.util.logging.Logger" %>
<jsp:useBean id="obj" class="login.RegisterDetails"/>
<jsp:setProperty property="*" name="obj"/>

<%
    Logger logger = Logger.getLogger(getClass().getName());
    logger.severe("Test logger");
    //Create User object
    User user = new User();
    user.setUserID(Integer.toString(CustomersDao.getNextUserID()));

    user.setUsername(obj.getUsername());

    user.setCustomerID(Integer.toString(CustomersDao.getNextCustomerID()));

    if(obj.getPassword1().equals(obj.getPassword2())) {
        user.setPassword(obj.getPassword1());
    }

    user.setRoleID("3");

    if(obj.getEmail1().equals(obj.getEmail2())) {
        user.setEmail(obj.getEmail1());
    }
    //End of User Object
    //Create Customer Object
    Customer customer = new Customer();
    customer.setCustomerID(Integer.toString(CustomersDao.getNextCustomerID()));
    customer.setAddressLine1(obj.getAddressLine1());
    customer.setAddressLine2(obj.getAddressLine2());
    customer.setPostcode(obj.getPostCode());
    customer.setCity(obj.getCity());
    customer.setCustomerEmail(obj.getEmail1());
    customer.setCustomerFirstName(obj.getFirstName());
    customer.setCustomerLastName(obj.getLastName());
    customer.setCountyID(obj.getCounty());
    //End of Customer Object

    boolean status = CustomersDao.register(user, customer);

    if (status){
        out.println("You have successfully registered");
        session.setAttribute("session","TRUE");
        session.setAttribute("userName", obj.getUsername());
%>
<meta http-equiv="Refresh" content="3;url=index.jsp">
<%  }
    else
    {
        out.print("Oops, There was an error registering your account, please try again.");

        %>
<meta http-equiv="Refresh" content="3;url=register.jsp">
<%
    }
%>