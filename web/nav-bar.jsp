<div class="nav-bar-wrapper">
    <ul id="nav-bar-ul">
        <li class="nav-bar-li"><a class="active" href="index.jsp">Home</a></li>
        <li class="nav-bar-li"><a href="my-tickets.jsp">My Tickets</a></li>
        <%
        if ((session.getAttribute("userName") == null) || (session.getAttribute("userName") == "")) {
        %>
            <div class="user-name" onclick="location.href='login.jsp'"><a id="user">Log In</a></div>
        <%} else {
        %>
            <div class="user-name"><a id="user"><%=session.getAttribute("userName") %>, <a onclick="location.href='logoutprocess.jsp'">Log Out</a></a></div>
        <%
        }
        %>
        <div class="nav-search-form">
            <input class = "nav-search-button" type="button" name = "search" value="Search" onclick="search()">
            <input type="text" name="search" id="autocomplete" style="width:400px">
        </div>
    </ul>
</div>