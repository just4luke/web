<%@ page import="dao.CountiesDao" %>
<%@ page import="models.County" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/site.css">
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.css">
    <script src="https://cdn.jsdelivr.net/jquery.jcider/latest/jcider.min.js"></script>
    <script type="text/javascript" src="js/site.js"></script>
    <title>Register</title>
</head>
<body>
<%@ include file="nav-bar.jsp" %>
<br/>
    <div class="main-content" id="login-register-content">
        <h3>Register</h3>
        <form action="registerprocess.jsp" method="post">
            <table class="login-form-table">
                <tr><td><a>Username: </a></td><td><input type="text" name="username"><br/><br/></td></tr>
                <tr><td>First Name: </td><td><input type="text" name="firstName"><br/><br/></td></tr>
                <tr><td>Last Name: </td><td><input type="text" name="lastName"><br/><br/></td></tr>
                <tr><td>County: </td><td><select  name="county">
                    <%
                        for(County county : CountiesDao.getCounties()){
                            out.print(county.getCountyID());
                    %>
                    <option value="<%=county.getCountyID()%>"><%=county.getCountyName()%></option>
                    <%
                    }
                    %>
                </select><br/><br/></td></tr>
                <tr><td>City: </td><td><input type="text" name="city"><br/><br/></td></tr>
                <tr><td>Address Line 1: </td><td><input type="text" name="addressLine1"><br/><br/></td></tr>
                <tr><td>Address Line 2: </td><td><input type="text" name="addressLine2"><br/><br/></td></tr>
                <tr><td>Postcode: </td><td><input type="text" name="postCode"><br/><br/></td></tr>
                <tr><td>Email: </td><td><input type="text" name="email1"><br/><br/></td></tr>
                <tr><td>Email: </td><td><input type="text" name="email2"><br/><br/></td></tr>
                <tr><td>Password: </td><td><input type="password" name="password1"><br/><br/></td></tr>
                <tr><td>Password: </td><td><input type="password" name="password2"><br/><br/></td></tr>
            </table>
            <input type="submit" value="Register">
        </form>
    </div>
</body>
</html>
